<?php

namespace App\Http\Controllers;


use App\City;
use App\NewUser;
use App\Province;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function insertUser(Request $request){
        $request = $request->json()->all();
        $mobile=$request["mobile"];
        if(NewUser::where("mobile",$mobile)->count()<1){
            $user["name"]=$request["first_name"]." ".$request["last_name"];
            $user["mobile"]=$mobile;
            //$user["email"]=$request["email"];
            $user["password"]=sha1($request["password"]);
            $user["token"]=sha1(self::generateTrackingCode());
            $user["is_active"]=1;

            if(NewUser::create($user)){
                return response(['status' =>true,"token"=>$user["token"]],200);
            }else{
                return response(['status' =>false,"message"=>"error"],200);
            }
        }else{
            $user=NewUser::where("mobile",$mobile)->first();
            if($user){
                $user=$user->toArray();
                return response(['status' =>true,"token"=>$user["token"]],200);
            }
        }
        return response(['status' =>false,"message"=>"error"],200);
    }

    public function login(Request $request){
        $mobile=$request->input("mobile");
        $password=$request->input("password");

        $user=NewUser::where(["mobile"=>$mobile,"password"=>sha1($password),"is_active"=>1])->first();
        if($user){
            return response(['status' =>true,"token"=>$user->token],200);
        }else{
            return response(['status' =>false,"message"=>"not exists"],200);
        }
    }

    public static function generateTrackingCode(){
        $characters = '0123456789KQRUVWXZPHL';
        $charactersLength = strlen($characters);
        $unique=false;
        while($unique!=true){
            $randomString=null;
            for ($i = 0; $i < 7; $i++) {
                $randomString = $characters[rand(0, $charactersLength - 1)];
            }
            if(NewUser::where("token",sha1($randomString))->count()>0){
                $unique=false;
            }else{
                $unique=true;
            }
        }
        return $randomString;
    }
}
