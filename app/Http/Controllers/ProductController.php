<?php

namespace App\Http\Controllers;


use App\City;
use App\NewUser;
use App\Product;
use App\Province;
use App\SubCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function listProduct(Request $request){
        $page = $request->input('page') ?: 0;
        $count = $request->input('count') ?: 30;
        $search = $request->input('search') ?: null;
        $province = $request->input('province') ?: null;
        $category = $request->input('category') ?: null;
        $sub_category = $request->input('sub_category') ?: null;

        $product=Product::
            take((int)$count)
            ->offset(((int)$page-1) * $count);

        if($search!=null){
            $product->where("name", "like", "%".$search."%")
                ->orWhere("tags", "like", "%".$search."%");
        }

        if($category!=null){
            $sub_category_ids=SubCategory::whereIn("category_id",$category)->pluck("id")->get();
            if($sub_category_ids){
                $product->whereIn("sub_category_id",$sub_category_ids);
            }
        }

        if($province!=null){
            $city_ids=City::whereIn("province_id",$province)->pluck("id")->get();
            if($city_ids){
                $product->whereIn("city_id",$city_ids);
            }
        }

        if($sub_category){
            $product->whereIn("sub_category_id",$sub_category);
        }

        $product->select("id","name","price","images")
        ->where("is_active",1);

        if($product = $product->get()){
            return response(['status' =>true,"result"=>$product->toArray()],200);
        }else{
            return response(['status' =>true,"result"=>null],200);
        }
    }

    public function getProduct(Request $request){
        $id=$request->input("id");
        $product=Product::where("id",$id)->where("is_active",1)->first();
        if($product){
            $export=clone $product;
            $export->toArray();
            $export["city_name"]=$product->city->name;
            $export["sub_category_name"]=$product->subCategory->name;
            $export["category_name"]=$product->subCategory->category->name;
            $export["category_id"]=$product->subCategory->category_id;
            $export["user_name"]=$product->user->name;
            return response(['status' =>true,"result"=>$export],200);
        }else{
            return response(['status' =>true,"result"=>null],200);
        }
    }

    public function setProduct(Request $request){
        $request = $request->json()->all();

        $user=NewUser::getUser($request["token"]);
        if($user){
            $product=null;

            if(array_key_exists("images",$request)){
                $product["images"]=$request["images"];
            }

            if(array_key_exists("attribute",$request)){
                $product["attribute"]=$request["attribute"];
            }

            $product["user_id"]=$user["id"];

            if(array_key_exists("city_id",$request)){
                $product["city_id"]=$request["city_id"];
            }

            if(array_key_exists("sub_category_id",$request)){
                $product["sub_category_id"]=$request["sub_category_id"];
            }

            if(array_key_exists("price",$request)){
                $product["price"]=$request["price"];
            }

            if(array_key_exists("description",$request)){
                $product["description"]=$request["description"];
            }

            if(array_key_exists("name",$request)){
                $product["name"]=$request["name"];
            }

            if(array_key_exists("tags",$request)){
                $product["tags"]=$request["tags"];
            }

            $product["is_active"]=1;

            $result=Product::create($product);
            if($result){
                return response(['status' =>true,"result"=>$result],200);
            }else{
                return response(['status' =>false,"result"=>"error"],200);
            }
        }
    }

    public function updateProduct(Request $request){
        $request = $request->json()->all();
        $user=NewUser::getUser($request["token"]);
        if($user){
            $product=null;
            $product["images"]=json_encode($request["images"]);
            $product["attribute"]=json_encode($request["attribute"]);
            $product["user_id"]=$user["id"];
            $product["city_id"]=$request["city_id"];
            $product["sub_category_id"]=$request["sub_category_id"];
            $product["price"]=$request["price"];
            $product["description"]=$request["description"];
            $product["name"]=$request["name"];
            $product["tags"]=$request["tags"];
            $product["is_active"]=1;

            $result=Product::where("id",$request["id"])->update($product);
            if($result){
                return response(['status' =>true,"result"=>$result],200);
            }else{
                return response(['status' =>false,"result"=>"error"],200);
            }
        }
    }

}
