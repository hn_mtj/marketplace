<?php

namespace App\Http\Controllers;


use App\City;
use App\NewUser;
use App\Order;
use App\OrderList;
use App\Product;
use App\Province;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function submitOrder(Request $request){
        $request = $request->json()->all();
        $user=NewUser::getUser($request["token"]);
        if($user){
          $order["user_id"]=$user["id"];
          $order["delivery_address"]=$request["delivery_address"];
          $order["city_id"]=$request["city_id"];
          $order["time_order"]=date("Y-m-d H:i:s");
          $order["payment_method"]=$request["payment_method"];
          $order["status"]=1 ;
          $order["tracking_code"]=self::generateTrackingCode();
          $total_price=0;
          $order_save=Order::create($order);
          foreach($request["products"] as $row){
              $product=Product::where("id",$row["id"])->first();
              if($product){
                  $product=$product->toArray();
                  $order_list["product_id"]=$row["id"];
                  $order_list["qty"]=$row["qty"];
                  $order_list["price"]=$product["price"];
                  $order_list["order_id"]=$order_save->id;
                  $total_price+=$order_list["price"];
                  OrderList::create($order_list);
              }
          }
            $order_save->update(["total_price"=>$total_price]);
            return response(['status' =>true,"total_price"=>$total_price,"tracking_code"=>$order["tracking_code"]],200);
        }
    }

    public static function generateTrackingCode(){
        $characters = '0123456789KQRUVWXZPHL';
        $charactersLength = strlen($characters);
        $unique=false;
        while($unique!=true){
            $randomString=null;
            for ($i = 0; $i < 7; $i++) {
                $randomString = $characters[rand(0, $charactersLength - 1)];
            }
            if(Order::where("tracking_code",$randomString)->count()>0){
                $unique=false;
            }else{
                $unique=true;
            }
        }
        return $randomString;
    }
}
