<?php

namespace App\Http\Controllers;


use App\City;
use App\Province;
use Illuminate\Http\Request;

class BasicDataController extends Controller
{
    public function listProvince(){
        $province=Province::get();
        if($province){
            return response(['status' =>true,"result"=>$province->toArray()],200);
        }else{
            return response(['status' =>true,"result"=>null],200);
        }
    }

    public function listCity(Request $request){
        $province_id=$request->input("province_id");
        $city=City::where("province_id",$province_id)->get();

        if($city){
            return response(['status' =>true,"result"=>$city->toArray()],200);
        }else{
            return response(['status' =>true,"result"=>null],200);
        }
    }


    public function uploadImage(Request $request){

        $path = $request->image->store('images');
        $export="shahrvandniyaz.ir/".$path;

        return response(['status' =>true,"path"=>$export],200);
    }
}
