<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=[
        "id",
        "name",
        "description",
        "price",
        "images",
        "sub_category_id",
        "city_id",
        "attribute",
        "user_id",
        "tags",
        "is_active"
    ];

    protected $casts = [
        'images' => 'array'
    ];


    public function subCategory()
    {
        return $this->belongsTo('App\SubCategory');
    }


    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function user()
    {
        return $this->belongsTo('App\NewUser');
    }
}


