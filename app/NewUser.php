<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewUser extends Model
{
    protected $fillable=[
        "id",
        "name",
        "mobile",
        "email",
        "password",
        "token",
        "is_active"
    ];

    protected $table="users";

    public static function getUser($token){
        $user=static::where("token",$token)->first();

        //

        //
        if($user){
            return $user->toArray();
        }else{
            return null;
        }
    }
}
