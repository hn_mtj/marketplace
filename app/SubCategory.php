<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable=[
        "id",
        "name",
        "image_path",
        "category_id"
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
