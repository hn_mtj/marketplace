<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=[
        "id",
        "user_id",
        "total_price",
        "time_order",
        "delivery_address",
        "city_id",
        "shipping_method",
        "payment_method",
        "status",
        "tracking_code"
    ];
}
