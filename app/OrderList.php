<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderList extends Model
{
    protected $fillable=[
        "id",
        "order_id",
        "product_id",
        "price",
        "qty"

    ];
}
