<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post("image/upload",'BasicDataController@uploadImage');

Route::get("category/list",'CategoryController@index');
Route::get("sub-category/list",'CategoryController@subCategory');

Route::get("province/list",'BasicDataController@listProvince');
Route::get("city/list",'BasicDataController@listCity');


Route::get("product/list",'ProductController@listProduct');
Route::get("product/get",'ProductController@getProduct');

Route::post("product/insert",'ProductController@setProduct');
Route::post("product/update",'ProductController@updateProduct');


Route::post("user/register",'UserController@insertUser');
Route::post("user/login",'UserController@login');


Route::post("order/checkout",'UserController@login');

